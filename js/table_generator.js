'use strict'
document.addEventListener("DOMContentLoaded", main);

function main() {
    let btn = document.querySelector('button');
    btn.addEventListener("click", generateTable);

}


function generateTable() {
    
    const table_space = document.querySelector("section#table-render-space");

    //create a table
    let new_table = document.createElement("table");
    table_space.appendChild(new_table);

    //get number of row and columns
    let num_row = document.getElementById("row-count").value;
    let num_col = document.getElementById("col-count").value;

    //create rows and td
    for (let i =0; i< num_row; i++) {

        let row = document.createElement("tr");

        //creating the td elements
        for (let j=0; j< num_col; j++) {
            let cell = document.createElement("td");
            let text = document.createTextNode(`Cell ${i+1} ${j+1} `);

            //append text to td then to row
            cell.appendChild(text);
            row.appendChild(cell);
        }
        //append to row to table
        new_table.appendChild(row);
    }
    //append table to the table-render-section
    


    //styling the table with functions

    changeTextColor(new_table);
    changeBackground(new_table);
    changeTagWidth(new_table);
    changeBorderColor(new_table);
    changeBorderWidth(new_table);



    //showing html code

    const html_space = document.querySelector("section#table-html-space");
    
    //create text area
    let text_area = document.createElement("textarea");
    text_area.value = new_table.innerHTML;
    html_space.appendChild(text_area);
}
