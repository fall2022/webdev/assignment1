/* */
'use strict'


//const btn = document.querySelector('button');

function changeTextColor(table) {
    let text_color = document.getElementById("text-color").value;
    table.style.color = text_color;
}

function changeBackground(table) {
    let bg_color = document.getElementById("bg-color").value;
    table.style.backgroundColor = bg_color;
}

function changeTagWidth(table) {
    let width = document.getElementById("table-width").value;
    table.style.width = `${width}%`;
}

function changeBorderColor(table) {
    let bd_color = document.getElementById("border-color").value;
    table.style.borderColor = bd_color;
}

function changeBorderWidth(table) {
    let bd_width = document.getElementById("border-width").value;
    table.style.borderWidth = `${bd_width}px`;
}